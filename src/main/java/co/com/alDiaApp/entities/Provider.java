package co.com.alDiaApp.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "providers")
@Getter @Setter
public class Provider implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_provider", nullable = false, unique = true)
    private Long idProvider;
    private String name;
    @Column(length = 80, nullable = false, unique = true)
    private String email;
    private String password;
    @Column(name = "api_key", nullable = false)
    private String apikey;
    private boolean enable;
    private String tokenPassword;
    public Provider() {}
    public Provider(String name, String email, String password, String apikey, boolean enable) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.apikey = apikey;
        this.enable =enable;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enable;
    }

}