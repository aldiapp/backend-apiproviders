package co.com.alDiaApp.exceptions;

public class ProviderNotFoundException extends Exception{
    public ProviderNotFoundException() {
        super("El provedor ya existe en la base de datos, vuelva a intentar!");
    }
    public ProviderNotFoundException(String message) {
        super(message);
    }
}
