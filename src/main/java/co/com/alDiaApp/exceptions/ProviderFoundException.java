package co.com.alDiaApp.exceptions;

public class ProviderFoundException extends Exception{
    public ProviderFoundException() {
        super("El Provedor que intenta registrar con ese nombre ya existe en la base de datos, vuelva a intentar!");
    }
    public ProviderFoundException(String message) {
        super(message);
    }

}
