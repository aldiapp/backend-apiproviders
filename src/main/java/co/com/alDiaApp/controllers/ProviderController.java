package co.com.alDiaApp.controllers;

import co.com.alDiaApp.DTO.*;
import co.com.alDiaApp.entities.Provider;
import co.com.alDiaApp.exceptions.AttributeException;
import co.com.alDiaApp.services.impl.ProviderEntityServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/providers")
@CrossOrigin(origins = "*", allowedHeaders = "*",exposedHeaders = "*")
public class ProviderController {

    @Autowired
    private ProviderEntityServiceImpl providerService;
    @PostMapping("/register")
    @ApiOperation("register a provider in the application")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Email already in use"),
    })
    public ResponseEntity<MessageDto> register(@Valid @RequestBody CreateProviderDto providerdto) throws AttributeException {
        Provider providerEntity = providerService.saveProvider(providerdto);
        return ResponseEntity.ok(new MessageDto(HttpStatus.OK, "provider " + providerEntity.getUsername() +
                " have been created, Api key: "+ providerEntity.getApikey() ));
    }

    //Actualizar api-key por ID
    @PutMapping("/{idProvider}/reset_api-key")
    @ApiOperation("update supplier api key")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Error, intenta nuevamente"),
    })
    public ResponseEntity<String> resetApikey(@PathVariable("idProvider") Long id) throws AttributeException {
        String apikey = providerService.resetApikey(id);
        return ResponseEntity.ok(apikey);
    }

   //Lista todos los provedores (Name)
    @GetMapping("/providers-name")
    @ApiOperation("list of name providers")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Error, intenta nuevamente"),
    })
    public ResponseEntity<List<String>> obtainNamesProviders() {
        List<String> names = providerService.obtainNamesProviders();
        return ResponseEntity.ok(names);
    }
    //Lista todos los provedores (id, name, email)
    @GetMapping("/all")
    @ApiOperation("List of providers id, name, email")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Error, intenta nuevamente"),
    })
    public ResponseEntity<List<ProviderResponseDTO>> getAllProviders() {
        List<ProviderResponseDTO> providers = providerService.getAllProviders();
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(providers.size()));
        System.out.println(ResponseEntity.ok().headers(headers).body(providers));
        return ResponseEntity.ok().headers(headers).body(providers);
        //return new ResponseEntity<>(providers, HttpStatus.OK);
    }

    //Obtine los suscriptores asociados a un provedor
    @GetMapping("/{id}/users")
    @ApiOperation("Gets all subscribers of a supplier")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Error, intenta nuevamente"),
    })
    public ResponseEntity<List<UserByProviderDTO>> getUsersByProvider(@PathVariable("id") Long idProvider) {
        ProviderRegisterDTO providerRegisterDTO = providerService.getProviderById(idProvider);

        if (providerRegisterDTO == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<UserByProviderDTO>> response = restTemplate.exchange(
                "http://users.aldia.site:8090/api-users/contract/provider",
                HttpMethod.POST,
                new HttpEntity<>(providerRegisterDTO, headers),
                new ParameterizedTypeReference<List<UserByProviderDTO>>() {});

        List<UserByProviderDTO> users = response.getBody();
        
        headers.add("X-Total-Count", String.valueOf(users.size()));
        System.out.println(ResponseEntity.ok().headers(headers).body(users));
        return ResponseEntity.ok().headers(headers).body(users);
        //return new ResponseEntity<>(users, HttpStatus.OK);
    }

    //Actuializacion de datos de un suscriptor
    @PostMapping("/updateContractNotification")
    @ApiOperation("Send the channel and service value to the users api to update the registry.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Error, intenta nuevamente"),
    })
    public ResponseEntity<ContractUpdateRequestDTO> updateContractNotification(@RequestBody ContractUpdateRequestDTO updateContractDTO) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpClient httpClient = HttpClients.createDefault();

        String url = "http://users.aldia.site:8090/" + updateContractDTO.getId() + "/updatens";

        HttpPatch httpPatch = new HttpPatch(url);

        String jsonPayload = new ObjectMapper().writeValueAsString(updateContractDTO);
        StringEntity entity = new StringEntity(jsonPayload, ContentType.APPLICATION_JSON);
        httpPatch.setEntity(entity);

        HttpResponse response = httpClient.execute(httpPatch);

        try {
            String responseBody = EntityUtils.toString(response.getEntity());

            EntityUtils.consume(response.getEntity());

            ObjectMapper objectMapper = new ObjectMapper();
            ContractUpdateRequestDTO contract = objectMapper.readValue(responseBody, ContractUpdateRequestDTO.class);

            return new ResponseEntity<>(contract, HttpStatus.OK);
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
    }

    //Notificaciones individuales
    @PostMapping("/sendIndividualNotify")
    @ApiOperation("sending of individual notifications.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Error, intenta nuevamente"),
    })
    public String sendIndividualNotify(@RequestBody UserByProviderDTO userByProviderDTO) {
        UserNotifyDTO userNotifyDTO = providerService.setNotifyObject(userByProviderDTO);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(
                "http://hub.aldia.site:8020/NotificationIndividual",
                new HttpEntity<>(userNotifyDTO, headers),
                Void.class);
        return "Notificacion enviada correctamente.";
    }

    //Notificaciones masiva
    @PostMapping("/sendMassiveNotify")
    @ApiOperation("sending of massive notifications.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Error, intenta nuevamente"),
    })
    public String sendMassiveNotify(@RequestBody List<UserByProviderDTO> users) {
        List<UserNotifyDTO> userNotifyDTOs = providerService.setNotifyObjects(users);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(
                "http://hub.aldia.site:8020/NotificationMassive",
                new HttpEntity<>(userNotifyDTOs, headers),
                Void.class);

        return "Notificaciones masivas enviadas correctamente.";
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProvider(@PathVariable("id") Long id) {
        providerService.deleteProvider(id);
        return ResponseEntity.ok("Provedor eliminado correctamente");
    }
}
