package co.com.alDiaApp.controllers;


import co.com.alDiaApp.DTO.JwtTokenDto;
import co.com.alDiaApp.DTO.LoginProviderDto;
import co.com.alDiaApp.config.jwt.JwtProvider;
import co.com.alDiaApp.exceptions.AttributeException;
import co.com.alDiaApp.services.impl.ProviderEntityServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthController {
    @Autowired
    private ProviderEntityServiceImpl providerEntityService;
    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/login")
    @ApiOperation("login a person in the application as a user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Bad Credentials"),
    })
    public ResponseEntity<JwtTokenDto> login(@ApiParam(value = "the provider's email and password", required = true)
                                             @Valid @RequestBody LoginProviderDto dto) throws AttributeException {
        JwtTokenDto jwtTokenDto = providerEntityService.login(dto);
        return ResponseEntity.ok(jwtTokenDto);
    }

    @PostMapping("/refresh-token")
    @ApiOperation("refresh token")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 401, message = "Unauthorized"),
    })
    public ResponseEntity<JwtTokenDto> refreshToken(@RequestBody JwtTokenDto jwtTokenDto) throws ParseException {
        String token = jwtProvider.refreshToken(jwtTokenDto);
        JwtTokenDto jwt = new JwtTokenDto(token);
        return new ResponseEntity(jwt,HttpStatus.OK);
    }
}
