package co.com.alDiaApp.repositories;

import co.com.alDiaApp.entities.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface ProviderRepository extends JpaRepository<Provider,Long> {
    boolean existsByEmail(String email);
    Optional<Provider> findByEmail(String email);
    Optional<Provider> findByTokenPassword(String tokenPassword);
}
