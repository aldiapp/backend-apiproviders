package co.com.alDiaApp.DTO;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
@Getter @Setter
public class CreateProviderDto {
    @NotBlank(message = "Email is mandatory")
    @Email(message = "Invalid email")
    private String email;
    @NotBlank(message = "Username is mandatory")
    private String password;
    private String name;
    private String apikey;

    public CreateProviderDto() {
    }

    public CreateProviderDto(String email, String password, String name, String apikey) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.apikey = apikey;
    }
}
