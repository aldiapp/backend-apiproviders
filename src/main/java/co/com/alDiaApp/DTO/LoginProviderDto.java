package co.com.alDiaApp.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
@Getter @Setter
public class LoginProviderDto {
    @NotBlank(message = "email is mandatory")
    private String email;
    @NotBlank(message = "email is mandatory")
    private String password;

    public LoginProviderDto() {
    }

    public LoginProviderDto(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
