package co.com.alDiaApp.DTO;

public class UserByProviderDTO {
    private Long id;
    private String email;
    private String phone;
    private Integer notificationSetting;
    private String subscriptionCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getNotificationSetting() {
        return notificationSetting;
    }

    public void setNotificationSetting(Integer notificationSetting) {
        this.notificationSetting = notificationSetting;
    }

    public String getSubscriptionCode() {
        return subscriptionCode;
    }

    public void setSubscriptionCode(String subscriptionCode) {
        this.subscriptionCode = subscriptionCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    private String name;
    private String lastname;

}