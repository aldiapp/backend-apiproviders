package co.com.alDiaApp.DTO;

import java.math.BigDecimal;

public class ContractUpdateRequestDTO extends ProviderRegisterDTO{

        private Long id;
        private Integer notificationSetting;
        private BigDecimal value;

    public ContractUpdateRequestDTO(Long idProvider, String apiKey, Long id, Integer notificationSetting, BigDecimal value) {
        super(idProvider, apiKey);
        this.id = id;
        this.notificationSetting = notificationSetting;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNotificationSetting() {
        return notificationSetting;
    }

    public void setNotificationSetting(Integer notificationSetting) {
        this.notificationSetting = notificationSetting;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
