package co.com.alDiaApp.DTO;

public class ProviderRegisterDTO {
    private Long idProvider;
    private String apikey;

    public ProviderRegisterDTO(Long idProvider, String apikey) {
        this.idProvider = idProvider;
        this.apikey = apikey;
    }

    public Long getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(Long idProvider) {
        this.idProvider = idProvider;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apiKey) {
        this.apikey = apiKey;
    }
}
