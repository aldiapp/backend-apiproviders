package co.com.alDiaApp.services.impl;

import co.com.alDiaApp.entities.Provider;
import co.com.alDiaApp.repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProviderDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private ProviderRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Provider> userEntity = this.userRepository.findByEmail(email);
        if(!userEntity.isPresent()){
            throw new UsernameNotFoundException("User doesn't exist");
        }
        return userEntity.get();
    }
}
