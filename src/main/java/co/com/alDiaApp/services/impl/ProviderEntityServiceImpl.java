package co.com.alDiaApp.services.impl;


import co.com.alDiaApp.DTO.*;
import co.com.alDiaApp.config.jwt.JwtProvider;
import co.com.alDiaApp.entities.Provider;
import co.com.alDiaApp.exceptions.AttributeException;
import co.com.alDiaApp.repositories.ProviderRepository;
import co.com.alDiaApp.services.ProviderServiceInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
public class ProviderEntityServiceImpl implements ProviderServiceInt {
    @Autowired
    private ProviderRepository providerEntityRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public Provider saveProvider(CreateProviderDto dto) throws AttributeException {
        if(providerEntityRepository.existsByEmail(dto.getEmail()))
            throw new AttributeException("Email already in use");
        return providerEntityRepository.save(mapProviderFromDto(dto));
    }

    public JwtTokenDto login(LoginProviderDto dto){
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(dto.getEmail(), dto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateToken(authentication);
        return new JwtTokenDto(token);
    }

    @Override
    public Optional<Provider> getByTokenPassword(String tokenPassword) {
        return providerEntityRepository.findByTokenPassword(tokenPassword);
    }

    // private methods
    private Provider mapProviderFromDto(CreateProviderDto dto) {
        String password = passwordEncoder.encode(dto.getPassword());
        return new Provider(dto.getName(), dto.getEmail(), password, generarApiKey(), true);
    }
    public String generarApiKey() {
        return UUID.randomUUID().toString();
    }

    public String resetApikey(Long id){
        Provider provider = providerEntityRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Provider not found: " + id));

        String newApiKey = generarApiKey();
        provider.setApikey(newApiKey);
        providerEntityRepository.save(provider);
        return newApiKey;
    }

    public List<String> obtainNamesProviders() {
        List<Provider> proveedores = providerEntityRepository.findAll();
        List<String> names = new ArrayList<>();

        for (Provider proveedor : proveedores) {
            names.add(proveedor.getName());
        }

        return names;
    }

    public List<ProviderResponseDTO> getAllProviders() {
        List<Provider> providers = providerEntityRepository.findAll();
        List<ProviderResponseDTO> providerDTOs = new ArrayList<>();

        for (Provider provider : providers) {
            ProviderResponseDTO providerResponseDTO = new ProviderResponseDTO(provider.getIdProvider(),provider.getName(),provider.getEmail());
            providerDTOs.add(providerResponseDTO);
        }
        return providerDTOs;
    }

    public ProviderRegisterDTO getProviderById(Long idProvider) {
        Provider provider = providerEntityRepository.findById(idProvider)
                .orElse(null);

        ProviderRegisterDTO providerRegisterDTO = null;
        if (provider != null) {
            providerRegisterDTO = new ProviderRegisterDTO(provider.getIdProvider(), provider.getApikey());
        }
        return providerRegisterDTO;
    }

    public UserNotifyDTO setNotifyObject(UserByProviderDTO userByProviderDTO){
        UserNotifyDTO userNotifyDTO = new UserNotifyDTO();
        userNotifyDTO.setChannel(userByProviderDTO.getNotificationSetting().toString());
        userNotifyDTO.setDestination(userByProviderDTO.getNotificationSetting().toString() == "email" ? userByProviderDTO.getEmail() : userByProviderDTO.getPhone());
        userNotifyDTO.setMessage("Señor usuario, recuerde pagar su servicio del contrato: " + userByProviderDTO.getId());
        return userNotifyDTO;
    }

    public List<UserNotifyDTO> setNotifyObjects(List<UserByProviderDTO> users) {
        List<UserNotifyDTO> userNotifyDTOs = new ArrayList<>();

        for (UserByProviderDTO user : users) {
            UserNotifyDTO userNotifyDTO = new UserNotifyDTO();
            userNotifyDTO.setChannel(user.getNotificationSetting().toString());
            userNotifyDTO.setDestination(user.getNotificationSetting().toString().equals("email") ? user.getEmail() : user.getPhone());
            userNotifyDTO.setMessage("Señor usuario, recuerde pagar su servicio del contrato: " + user.getId());

            userNotifyDTOs.add(userNotifyDTO);
        }

        return userNotifyDTOs;
    }

    @Override
    public void deleteProvider(Long providerId) {
        providerEntityRepository.deleteById(providerId);
    }

    @Override
    public Optional<Provider> getProvider(String email) {
        return providerEntityRepository.findByEmail(email);
    }

    @Override
    public Provider updateProvider(Provider providerEntity) throws AttributeException {
        return providerEntityRepository.save(providerEntity);
    }


}
