package co.com.alDiaApp.services;

import co.com.alDiaApp.DTO.CreateProviderDto;
import co.com.alDiaApp.DTO.JwtTokenDto;
import co.com.alDiaApp.DTO.LoginProviderDto;
import co.com.alDiaApp.entities.Provider;
import co.com.alDiaApp.exceptions.AttributeException;

import java.util.Optional;


public interface ProviderServiceInt {
    public Provider saveProvider(CreateProviderDto dto) throws AttributeException;
    public Optional<Provider> getProvider(String email);
    public Provider updateProvider(Provider providerEntity) throws AttributeException;
    public void deleteProvider(Long userId);
    public JwtTokenDto login(LoginProviderDto dto);
    public Optional<Provider> getByTokenPassword(String tokenPassword);
}
